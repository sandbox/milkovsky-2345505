<?php

/**
 * Multistep-form pane callback: Name pane.
 */
function mf_example_name_pane_form($form, &$form_state, &$values, $pane) {
  $pane_form = array();

  $pane_form['first_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#description' => t('Write your first name here'),
    '#required' => TRUE,
    '#default_value' => !empty($values['first_name']) ? $values['first_name'] : '',
  );
  $pane_form['last_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Last name'),
    '#description' => t('Write your last name here'),
    '#required' => TRUE,
    '#default_value' => !empty($values['last_name']) ? $values['last_name'] : '',
  );

  return $pane_form;
}

/**
 * Multistep-form pane callback: Middle name pane.
 */
function mf_example_extra_names_pane_form($form, &$form_state, &$values, $pane) {
  $pane_form = array();

  $pane_form['middle_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Middle Name'),
    '#description' => t('Write your middle name here if you have it'),
    '#default_value' => !empty($values['middle_name']) ? $values['middle_name'] : '',
  );

  return $pane_form;
}

/**
 * Multistep-form pane callback: Other data pane.
 */
function mf_example_other_pane_form($form, &$form_state, &$values, $pane) {
  $pane_form = array();

  $pane_form['age'] = array(
    '#type' => 'textfield',
    '#title' => t('Age'),
    '#description' => t('Write you age here please'),
    '#element_validate' => array('element_validate_integer_positive'),
    '#default_value' => !empty($values['age']) ? $values['age'] : '',
  );

  $pane_form['ajax_button'] = array(
    '#type' => 'button',
    '#value' => t('Click me to check your age'),
    '#limit_validation_errors' => array(),
    '#ajax' => array(
      'wrapper' => 'mf-form-wrapper',
      'callback' => 'mf_step_sample_ajax_callback',
    ),
    '#prefix' => 'I am sample ajax button inside multistep form ',
    '#suffix' => '<br><br>',
  );

  return $pane_form;
}

/**
 * Sample ajax button submit callback.
 */
function mf_step_sample_ajax_callback($form, &$form_state) {
  $name = $form_state['storage']['values']['personal_info']['name']['first_name'];
  if (empty($form_state['values']['other_data']['other']['age'])) {
    drupal_set_message("Ajax callback:<br>$name, you have not entered your age");
  }
  else {
    $age = $form_state['values']['other_data']['other']['age'];
      if (is_numeric($age) && $age > 0) {
      drupal_set_message("Ajax callback:<br>$name, your age is $age");
    }
    else {
      $age = $form_state['values']['other_data']['other']['age'];
      drupal_set_message("Ajax callback:<br>$name, are you sure that '$age' is a valid age?");
    }
  }
  return $form;
}

/**
 * Multistep-form pane callback: Review pane.
 */
function mf_example_review_pane_form($form, &$form_state, &$values, $pane) {
  $pane_form = array();
  $message = '<strong>Entered values</strong>: <br/>';
  $message .= '<br/>';
  foreach ($form_state['storage']['values'] as $step => $values) {
    $message .= "$step: <br/>";
    foreach ($values as $key => $value) {
      if ($key == 'steps') {
        continue;
      }
      $output = '';
      if (is_array($value)) {
        foreach ($value as $k => $val) {
          if ($k == 'ajax_button') {
            continue;
          }
          $output .= $k . '=' . ($val ? $val . '; ' : '') . '<br/>';
        }
      }
      $message .= "$key: $output";
    }
    $message .= '<br/>';
  }
  $message .= '<br/>';

  $pane_form['results'] = array(
    '#markup' => $message,
  );

  return $pane_form;
}

/**
 * Multistep-form pane callback: Completed pane.
 */
function mf_example_completed_pane_form($form, &$form_state, &$values, $pane) {
  $pane_form = array();

  $pane_form['completed'] = array(
    '#type' => 'item',
    '#title' => t('Congrats!'),
    '#markup' => t('You have completed the multistep form.'),
  );
  $pane_form['try'] = array(
    '#type' => 'item',
    '#markup' => l(t('Try again?'), 'mf-example'),
  );

  return $pane_form;
}
