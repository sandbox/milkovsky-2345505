<?php
/**
 * @file
 * Default panes for multistep forms.
 */

/**
 * Implements hook_mf_pane_info().
 */
function mf_mf_pane_info() {
  $panes = array();

  $panes['title'] = array(
    'title' => t('Title'),
    'file' => 'includes/panes.inc',
    'fieldset' => FALSE,
  );

  $panes['steps'] = array(
    'title' => t('Steps'),
    'file' => 'includes/panes.inc',
    'fieldset' => FALSE,
  );

  return $panes;
}

/**
 * Multistep-form pane callback: Step title.
 */
function mf_title_pane_form($form, &$form_state, &$values, $pane) {
  $pane_form = array();
  $step = $form_state['storage']['step'];

  $mf_id = $form_state['storage']['mf_id'];
  $steps = mf_steps($mf_id);
  $index = array_search($step['step_id'], array_keys($steps)) + 1;

  $title = t('Step !index: !title', array('!index' => $index, '!title' => $step['title']));
  $pane_form['title'] = array(
    '#markup' => '<h2>' . $title  . '</h2>',
  );
  return $pane_form;
}

/**
 * Multistep-form pane callback: Steps pane.
 */
function mf_steps_pane_form($form, &$form_state, &$values, $pane) {

  $mf_id = $form_state['storage']['mf_id'];
  $current_step = $form_state['storage']['step'];
  $pane_form = mf_steps_build_buttons($mf_id, $current_step);

  return $pane_form;
}

function mf_steps_build_buttons($mf_id, $current_step) {
  $buttons = array();

  $steps = mf_steps($mf_id);

  $disabled = FALSE;
  foreach ($steps as $step_id => $step) {

    $class = array(drupal_html_class($step['step_id']));

    // Step is current.
    if ($step_id === $current_step['step_id']) {
      $class[] = 'active';
      // Steps after current should not be clickable.
      $disabled = TRUE;
    }

    $buttons[$step_id] = array(
      '#type' => 'submit',
      '#value' => t($step['title']),
      '#limit_validation_errors' => array(),
      '#attributes' => array('class' => $class),
      '#submit' => array('mf_step_ajax_callback'),
      '#disabled' => $disabled,
      '#ajax' => array(
        'wrapper' => 'mf-form-wrapper',
        'callback' => 'mf_step_ajax_callback', // todo: leave only 1 callback.
      ),
    );
  }

  return $buttons;
}

/**
 * Step button submit callback: Navigate to step.
 */
function mf_step_ajax_callback($form, &$form_state) {

  $mf_id = $form_state['storage']['mf_id'];
  $step_id = end($form_state['triggering_element']['#array_parents']);
  $form_state['storage']['step'] = mf_step_load($mf_id, $step_id);

  // If the data for the next step has been entered, pass it to the form.
  if (!empty($form_state['storage']['values'][$step_id])) {
    $form_state['values'][$step_id] = $form_state['storage']['values'][$step_id];
  }

  $form_state['rebuild'] = TRUE;
  return $form;
}
